var app = angular.module('myApp',[]);
app.controller('myCtrl',function($scope,$http) {
    $scope.reloaded = function() {
        $http.get("http://localhost/startbootstrap-sb-admin-2-1.0.8/pages/cardstate/cardstate.php").then(function(response){
            $scope.index = 0;
            $scope.cards = response.data.cardstatement;
            $scope.length = $scope.cards.length;
            $scope.zero = function() {
                if($scope.index < 0) {$scope.index = 0;}
                if($scope.index >= $scope.length-1) {$scope.index = $scope.length-1;}
                $scope.transno = $scope.cards[$scope.index].transno;
                $scope.uid = $scope.cards[$scope.index].uid;
                $scope.number = $scope.cards[$scope.index].number;
                $scope.date = $scope.cards[$scope.index].date;
                $scope.sellerno = $scope.cards[$scope.index].sellerno;
                $scope.product = $scope.cards[$scope.index].product;
                $scope.price = $scope.cards[$scope.index].price;
            };
            $scope.fwd = function() {$scope.index += 1; $scope.zero();};
            $scope.bwd = function() {$scope.index -= 1; $scope.zero();};
            $scope.zero();
        });
    };
    $scope.reloaded();
});