<?php require_once './login/success.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EGCO423: Dasboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
            </div>
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="personhtml.php"><i class="fa fa-table fa-fw"></i> SQL Table</a>
                        </li>
                        <li>
                            <a href="personjson.php"><i class="fa fa-table fa-fw"></i> JSON Table </a>
                        </li>
                        <li>
                            <a href="cardstate.php"><i class="fa fa-edit fa-fw"></i> Card Statement </a>
                        </li>
                        <li>
                            <a href="listtransactions.php"><i class="fa fa-wrench fa-fw"></i> Transactions </a>
                        </li>
                        <li>
                            <a href="searchnews.php"><i class="fa fa-sitemap fa-fw"></i> Search News </a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper" style="min-height: 355px;">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header" style="color:#0066CC">Credit Card Statement</h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>

			<div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit Card Transaction
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" name="card" action="cardstate/insertcard.php" method="post">
                                        <!--
                                        <div class="form-group">
                                            <label>Transaction No.</label>
                                            <input  name="transno" class="form-control" placeholder="Enter text"  value="" disabled>
                                        </div>

										<label>User ID No.</label>
                                        <div class="form-group input-group">
											<span class="input-group-addon">@</span>
                                            <input name="uid" type="text" class="form-control" placeholder="User ####" value="" disabled>
                                        </div>
                                        -->
										<div class="form-group">
                                            <label>Credit Card No.</label>
                                            <input name="number" class="form-control" placeholder="Enter Creditcard No."  value="">
                                        </div>

										<div class="form-group">
                                            <label>Date</label>
                                            <input name="date" type="date" class="form-control" value="">
                                        </div>
                                    
										<div class="form-group">
                                            <label>Seller No.</label>
                                            <input id="sellerno" name="sellerno" class="form-control" placeholder="Enter Seller No." value="">
                                        </div>

										<div class="form-group">
                                            <label>Product</label>
                                            <input id="product" name="product" class="form-control" placeholder="Enter Product" value="">
                                        </div>

                                        <label>Price</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="price" name="price" type="text" class="form-control"   value="">
                                            <span class="input-group-addon">.00</span>
                                        </div>
										<table width='100%'><tr>
                                        <td width ='25%'></td>
										
										<td width ='50%'>	
												<button id="save"  name = "save" type="submit" class="btn btn-success btn-circle btn-lg"><i class="fa fa-save"></i></button>
												<button type="button" class="btn btn-danger btn-circle btn-lg" onClick="location.href='cardstate.php'" ><i class="fa fa-remove"></i></button>
										<td  width ='25%'></td>
										</tr></table>

                                    </form>
									
									</div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
